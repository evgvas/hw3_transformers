// 1. Создать класс `Transformer` со свойствами и методами:
//     - `name`
//     - `health` - по умолчанию имеет значение 100
// - `attack()` - будет в дальнейшем переопределен
// - `hit(weapon)` - метод, который обрабытывает логику атаки на данного трансформера, принимает на вход экземпляр класса `Weapon`, отнимает текущее здоровье трансформера на размер атаки (свойство `damage` и `Weapon`).
// 2. Создать класс `Autobot`, который наследуется от класса `Transformer`.
// - Имеет свойсто `weapon` (экземпляр класса `Weapon`), т.к. автоботы сражаются с использованием оружия.
// - Конструктор класса принимает 2 параметра: имя трансформера и оружее (экземпляр класса `Weapon`).
// - Метод `attack` возвращает результат использования оружия `weapon.fight()`
// 3. Создать класс `Deceptikon` который наследуется от класса `Transformer`.
// - Десептиконы не пользуются оружием, поэтому у них нет свойства `weapon`. За то они могут иметь разное количество здоровья.
// - Конструктор класса принимает 2 параметра: имя `name` и количество здоровья `health`
// - Метод `attack` возвращает характеристики стандартного вооружения, например, `{ damage: 5, speed: 1000 }`, где `damage` - это кличество урона, а `speed` - скорость атаки в мс.
// 4. Создать класс оружия `Weapon`, на вход принимает 2 параметра: `damage` - урон и `speed` - скорость атаки. Имеет 1 метод `fight`, который возвращает характеристики оружия в виде `{ damage: 5, speed: 1000 }`
// 5. Создать 1 автобота с именем `OptimusPrime` с оружием, имеющим характеристики `{ damage: 100, speed: 1000 }`
// 6. Создать 1 десептикона с именем `Megatron` и показателем здоровья `10000`
// 7. Посмотреть что происходит при вызове метода `atack()` у траснформеров разного типа, посмотреть сигнатуры классов
// 8. Вопрос: сколько нужно автоботов чтобы победить Мегатрона если параметр `speed` в оружии это количество милсекунд до следующего удара? Реализовать симуляцию боя.

class Transformer{
    constructor(name,health=100){
        this.name = name;
        this.health = health;
    }
    attack(){}
    hit(fight){
        this.health = this.health - fight.damage;
    }
}
class Autobot extends Transformer {
    constructor(name,weapon) {
        super(name);
        this.weapon = weapon;
    }
    attack() {
        return this.weapon.fight()
    }
}
class Deceptikon extends Transformer {
    attack() {
        return {damage:5,speed:1000};
    }
}
class weapon {
    constructor(damage,speed) {
        this.damage = damage;
        this.speed = speed;
    }
    fight(){
        return{
            damage:this.damage,
            speed:this.speed
        }
    }
}
class Arena{
    constructor(side1,side2,visualizer) {
        this.side1 = side1;
        this.side2 = side2;
        this.visualizer = visualizer;
    }
    start(){
        const timer = setInterval(()=> {
            this.side1.forEach(bot => this.side2[0].hit(bot.attack()));
            this.side2 = this.side2.filter(bot => bot.health > 0);
            if (!this.side2.length) {
                clearInterval(timer)
                alert('Transformers win')
            }
            this.side2.forEach(bot=>this.side1[0].hit(bot.attack()));
            this.side1 = this.side1.filter(bot => bot.health > 0);
            if (!this.side1.length) {
                clearInterval(timer);
                alert('Deceptikon win');
            }
            this.visualizer.render(
                this.side1.map(bot=>bot.health),
                this.side2.map(bot=>bot.health)
            );
        },100)
    }
}

class Visualizer {
    render(hps1, hps2) {
        document.querySelector('.arena-side-1').innerHTML = hps1.map(hp => `<div class="bot"><span>${hp} hp</span></div>`).join('');
        document.querySelector('.arena-side-2').innerHTML = hps2.map(hp => `<div class="bot"><span>${hp} hp</span></div>`).join('');
    }
}
const arena = new Arena (
    [new Autobot('OptimusPrime',new weapon(100,1500)),
        new Autobot('Bot-1',new weapon(50,1000)),
        new Autobot('Bot-2',new weapon(200,800))
    ],
    [new Deceptikon('Megatron',10000)],
    new Visualizer
)
arena.start()
